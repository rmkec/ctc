package com.example.ctc.Item;

public class JobItem {

    int jobID,duration;
    String jobCode,jobName,jobLocation,Cluster,structureName,elementName,username;
    String datejob;

    public JobItem(int jobID, String jobCode, String jobName, String jobLocation, String cluster, String structureName, String elementName,int duration,String username,String datejob) {
        this.jobID = jobID;
        this.duration = duration;
        this.jobCode = jobCode;
        this.jobName = jobName;
        this.jobLocation = jobLocation;
        Cluster = cluster;
        this.structureName = structureName;
        this.elementName = elementName;
        this.username=username;
        this.datejob=datejob;

    }

    public String getDatejob() {
        return datejob;
    }

    public void setDatejob(String datejob) {
        this.datejob = datejob;
    }

    public JobItem() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getJobID() {
        return jobID;
    }

    public void setJobID(int jobID) {
        this.jobID = jobID;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getJobCode() {
        return jobCode;
    }

    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobLocation() {
        return jobLocation;
    }

    public void setJobLocation(String jobLocation) {
        this.jobLocation = jobLocation;
    }

    public String getCluster() {
        return Cluster;
    }

    public void setCluster(String cluster) {
        Cluster = cluster;
    }

    public String getStructureName() {
        return structureName;
    }

    public void setStructureName(String structureName) {
        this.structureName = structureName;
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }
}