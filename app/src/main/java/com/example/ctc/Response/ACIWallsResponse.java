package com.example.ctc.Response;

public class ACIWallsResponse {
    double ccpmax;

    public ACIWallsResponse() {
        super();
    }

    public ACIWallsResponse(double ccpmax) {
        super();
        this.ccpmax = ccpmax;
    }

    public double getCcpmax()  {
        return ccpmax;

    }

    public void setCcpmax(double ccpmax) {
        this.ccpmax = ccpmax;
    }

    @Override
    public String toString() {
        return "ACIWallsResponse{" +
                "ccpmax=" + ccpmax +
                '}';
    }
}
