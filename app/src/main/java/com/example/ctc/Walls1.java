package com.example.ctc;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ctc.Response.ACIWallsResponse;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class Walls1 extends AppCompatActivity {

    String[] listCement,listvibration,listslag,listflyash,listheight,listslump,listelement,listretarders;
    Button mmbtn1,mmbtn2,mmbtn3,mmbtn4,mmbtn6,mmbtn7,mmbtn8,mmbuttonslump,mmbtn9,mbtnretard;
    TextView mmtext1,mmtext2,mmtext3,mmtext4,mmtext6,mmslump,mmtext9,ccpmax,mmretard;
    EditText volume,planAreawall,formheightwall,denwall;
    //EditText wallrop;











    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        setContentView(R.layout.activity_walls1);
        mmbtn9=findViewById(R.id.buttonelement);
        mbtnretard=findViewById(R.id.buttonretarder);
        mmbtn1=findViewById(R.id.buttoncement);
        mmbtn2=findViewById(R.id.buttonvibrate);
        mmbtn3=findViewById(R.id.buttonslag);
        mmbtn4=findViewById(R.id.buttonflyash);
        mmbtn6=findViewById(R.id.buttonheight);
        mmbuttonslump=findViewById(R.id.buttonslump);

        mmbtn7=findViewById(R.id.buttoncalculate);
        mmbtn8=findViewById(R.id.buttonclear);
        mmslump=findViewById(R.id.textslump);

        mmtext9=findViewById(R.id.textelement);
        mmretard=findViewById(R.id.textretarders);
        mmtext1=findViewById(R.id.textcement);
        mmtext2=findViewById(R.id.textvibrate);
        mmtext3=findViewById(R.id.textslag);
        mmtext4=findViewById(R.id.textflyash);
        mmtext6=findViewById(R.id.textheight);




        volume=findViewById(R.id.editvolume);
        planAreawall=findViewById(R.id.editplanwall);
        //tempwall=findViewById(R.id.edittempwall);
        formheightwall=findViewById(R.id.editverformheight);
        denwall=findViewById(R.id.editdensitywall);
        //wallrop=findViewById(R.id.editrop);

        ccpmax=findViewById(R.id.editccpmax);



        mmbtn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                volume.getText().clear();
                planAreawall.getText().clear();
                //tempwall.getText().clear();
                denwall.getText().clear();
                formheightwall.getText().clear();
                //wallrop.getText().clear();
               // slump.getText().clear();
            }
        });

        mmbtn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(volume.getText().length()==0 || formheightwall.getText().length()==0  ||
                        denwall.getText().length()==0  ||planAreawall.getText().length()==0  )
                {
                    Toast.makeText(getApplicationContext(),"Please verify the fields",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    final String wallelement=mmtext9.getText().toString();
                    final String wallcement=mmtext1.getText().toString();
                    final String wallinternal=mmtext2.getText().toString();
                    final String wallslag=mmtext3.getText().toString();
                    final String wallflyash=mmtext4.getText().toString();
                    final String wallretarders=mmretard.getText().toString();
                    final String wallslump=mmslump.getText().toString();
                    final String wallheighting=mmtext6.getText().toString();

                    final double wallvolume=Double.parseDouble(volume.getText().toString());
                    final double wallformheight=Double.parseDouble(formheightwall.getText().toString());
                    final double walldensity=Double.parseDouble(denwall.getText().toString());
                    final double wallplanarea=Double.parseDouble(planAreawall.getText().toString());
                    //final double walltemperature=Double.parseDouble(tempwall.getText().toString());
                    //final double wallrateofplacement=Double.parseDouble(wallrop.getText().toString());

                    // final String wallslump=slump.getText().toString();

                    AsyncHttpClient client = new AsyncHttpClient();
                    JSONObject jsonObject = new JSONObject();
                    StringEntity entity = null;

                    ACIWallsData aciWallsData=new ACIWallsData(
                            wallelement,
                            wallcement,
                            wallinternal,
                            wallslag,
                            wallflyash,
                            wallretarders,
                            wallslump,
                            wallheighting,
                            walldensity,
                            wallvolume,
                            wallformheight,
                            wallplanarea
                            //walltemperature
                            //wallrateofplacement
                            );

                    try
                    {
                        entity=new StringEntity("{}");
                        jsonObject.put("jobCode",preferences.getInt("Jobid",0));
                        jsonObject.put("userId",preferences.getInt("userID",0));
                        jsonObject.put("latitude",preferences.getString("Latitude",null));
                        jsonObject.put("longtiude",preferences.getString("Longitude",null));
                        jsonObject.put("retarders",aciWallsData.getRetarders());
                        jsonObject.put("elementType",aciWallsData.getElementType());
                        jsonObject.put("cementType",aciWallsData.getCementType());
                        jsonObject.put("internalVibration",aciWallsData.getInternalVibration());
                        jsonObject.put("slag",aciWallsData.getSlag());
                        jsonObject.put("flyash",aciWallsData.getFlyash());
                        jsonObject.put("slump",aciWallsData.getSlump());
                        jsonObject.put("wallheight",aciWallsData.getWallheight());
                        jsonObject.put("den",aciWallsData.getDen());
                        jsonObject.put("verticalformheight",aciWallsData.getVerticalformheight());
                        jsonObject.put("volume",aciWallsData.getVolume());
                        jsonObject.put("planareas",aciWallsData.getPlanareas());
                        //jsonObject.put("temperatureee",aciWallsData.getTemperatureee());
                        //jsonObject.put("rateofplacement",aciWallsData.getWallrop());


                        entity = new StringEntity(jsonObject.toString());
                        Toast.makeText(Walls1.this, "Result calculated successfully", Toast.LENGTH_SHORT).show();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    client.post(Walls1.this, "http://rmkeclandt-env.miurmysbmy.us-east-2.elasticbeanstalk.com/aci", entity, "application/json", new AsyncHttpResponseHandler() {
                        //client.get(Walls.this, "http://10.1.2.51:8080/DemoServlet", entity, "application/json", new AsyncHttpResponseHandler(){
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            try {
                                //// Log.d("Demo Success: ", new JSONObject(new String(responseBody, "UTF-8")).toString());
                                //// JSONObject jsonObject = new JSONObject(new String(responseBody, "UTF-8"));
                                //JSONObject temp=jsonObject.getJSONObject("pmax");
                                ////   CiriaResponse ciriaResponse = new CiriaResponse(jsonObject.getDouble("pmax"));
                                ////   Log.d("Demo pMax: ", ciriaResponse.toString());
                                ////    pmax.setText(Double.toString(ciriaResponse.getPmax()));
                                //  pmax.setText(jsonObject.getString("pmax"));
                                // pmax.setText((int) ciriaResponse.getPmax());


                                Log.d("Demo Success: ", new JSONObject(new String(responseBody, "UTF-8")).toString());
                                JSONObject jsonObject = new JSONObject(new String(responseBody, "UTF-8"));
                                ACIWallsResponse aciWallsResponse = new ACIWallsResponse(jsonObject.getDouble("ccpmax"));
                                Log.d("Demo ccPmax: ", aciWallsResponse.toString());
                                ccpmax.setText(Double.toString(aciWallsResponse.getCcpmax()));
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            try {
                                JSONObject jsonObject = new JSONObject(new String(responseBody, "UTF-8"));
                                Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });

        mmbtn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listelement=new String[]{"\nANY\n","\nWALLS\n","\nCOlUMNS\n"};
                AlertDialog.Builder mBuilder=new AlertDialog.Builder(Walls1.this);
                mBuilder.setTitle("Element type");//set title of AlertDialog
                mBuilder.setIcon(R.drawable.icon);//Set icon of AlertDialog

                mBuilder.setSingleChoiceItems(listelement, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i)
                    {
                        if(i==0)
                        {
                            mmtext9.setText("ANY");
                            dialog.dismiss();
                        }
                        if(i==1)
                        {
                            mmtext9.setText("WALLS");
                            dialog.dismiss();
                        }
                        if(i==2)
                        {
                            mmtext9.setText("COLUMNS");
                            dialog.dismiss();
                        }



                    }
                }); mBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                //Show Alert Dialog
                AlertDialog mDialog=mBuilder.create();
                mDialog.show();
            }
        });
        mmbuttonslump.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listslump=new String[]{"\n\n>175\n\n","\n\n<=175\n\n"};
                AlertDialog.Builder mBuilder=new AlertDialog.Builder(Walls1.this);
                mBuilder.setTitle("Slump");
                mBuilder.setIcon(R.drawable.icon);

                mBuilder.setSingleChoiceItems(listslump, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i)
                    {
                      if(i==0)
                      {
                          mmslump.setText(">175");
                          dialog.dismiss();
                      }

                      if(i==1)
                      {
                          mmslump.setText("<=175");
                          dialog.dismiss();
                      }


                    }
                });
                mBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                //Show Alert Dialog
                AlertDialog mDialog=mBuilder.create();
                mDialog.show();

            }
        });
        mbtnretard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listretarders=new String[]{"\n\nYes\n\n","\n\nNo\n\n"};
                AlertDialog.Builder mBuilder=new AlertDialog.Builder(Walls1.this);
                mBuilder.setTitle("RETARDERS");
                mBuilder.setIcon(R.drawable.icon);

                mBuilder.setSingleChoiceItems(listretarders, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i)
                    {
                        if(i==0)
                        {
                            mmretard.setText("Yes");
                            dialog.dismiss();

                        }

                        if(i==1)
                        {
                            mmretard.setText("No");
                            dialog.dismiss();
                        }


                    }
                });
                mBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                //Show Alert Dialog
                AlertDialog mDialog=mBuilder.create();
                mDialog.show();

            }
        });

        mmbtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listCement=new String[]{"\n\nType 1\n\n","\n\nType 2\n\n","\n\nType 3\n\n","\n\nAny Type\n\n"};
                AlertDialog.Builder mBuilder=new AlertDialog.Builder(Walls1.this);
                mBuilder.setTitle("Cement Type");//set title of AlertDialog
                mBuilder.setIcon(R.drawable.icon);//Set icon of AlertDialog

                mBuilder.setSingleChoiceItems(listCement, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i)
                    {
                          if(i==0)
                          {
                              mmtext1.setText("Type 1");
                              dialog.dismiss();
                          }
                        if(i==1)
                        {
                            mmtext1.setText("Type 2");
                            dialog.dismiss();
                        }
                        if(i==2)
                        {
                            mmtext1.setText("Type 3");
                            dialog.dismiss();
                        }
                        if(i==3)
                        {
                            mmtext1.setText("Any Type");
                            dialog.dismiss();
                        }


                    }
                }); mBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                //Show Alert Dialog
                AlertDialog mDialog=mBuilder.create();
                mDialog.show();
            }
        });
        mmbtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listvibration=new String[]{"\n>1.2\n","\n<=1.2\n","\nAny Type\n"};
                AlertDialog.Builder mBuilder=new AlertDialog.Builder(Walls1.this);
                mBuilder.setTitle("Internal Vibration Length");//set title of AlertDialog
                mBuilder.setIcon(R.drawable.icon);//Set icon of AlertDialog

                mBuilder.setSingleChoiceItems(listvibration, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i)
                    {
                        if(i==0)
                        {
                            mmtext2.setText(">1.2");
                            dialog.dismiss();
                        }
                        if(i==1)
                        {
                            mmtext2.setText("<=1.2");
                            dialog.dismiss();
                        }
                        if(i==2)
                        {
                            mmtext2.setText("Any Type");
                            dialog.dismiss();
                        }



                    }
                }); mBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                //Show Alert Dialog
                AlertDialog mDialog=mBuilder.create();
                mDialog.show();
            }
        });

        mmbtn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listslag=new String[]{"\nNone\n","\n<70\n","\n>=70\n"};
                AlertDialog.Builder mBuilder=new AlertDialog.Builder(Walls1.this);
                mBuilder.setTitle("Slag");//set title of AlertDialog
                mBuilder.setIcon(R.drawable.icon);//Set icon of AlertDialog

                mBuilder.setSingleChoiceItems(listslag, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i)
                    {
                        if(i==0)
                        {
                            mmtext3.setText("None");
                            dialog.dismiss();
                        }
                        if(i==1)
                        {
                            mmtext3.setText("<70");
                            dialog.dismiss();
                        }
                        if(i==2)
                        {
                            mmtext3.setText(">=70");
                            dialog.dismiss();
                        }




                    }
                }); mBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                //Show Alert Dialog
                AlertDialog mDialog=mBuilder.create();
                mDialog.show();
            }
        });
        mmbtn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listflyash=new String[]{"\nNone\n","\n<40\n","\n>=40\n"};
                AlertDialog.Builder mBuilder=new AlertDialog.Builder(Walls1.this);
                mBuilder.setTitle("Flyash");//set title of AlertDialog
                mBuilder.setIcon(R.drawable.icon);//Set icon of AlertDialog

                mBuilder.setSingleChoiceItems(listflyash, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i)
                    {
                        if(i==0)
                        {
                            mmtext4.setText("None");
                            dialog.dismiss();
                        }

                        if(i==1)
                        {
                            mmtext4.setText("<40");
                            dialog.dismiss();
                        }
                        if(i==2)
                        {
                            mmtext4.setText(">=40");
                            dialog.dismiss();
                        }




                    }
                }); mBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                //Show Alert Dialog
                AlertDialog mDialog=mBuilder.create();
                mDialog.show();
            }
        });
        mmbtn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listheight=new String[]{"\n>4.2\n","\n<=4.2\n","\nAny Type\n"};
                AlertDialog.Builder mBuilder=new AlertDialog.Builder(Walls1.this);
                mBuilder.setTitle("Height");//set title of AlertDialog
                mBuilder.setIcon(R.drawable.icon);//Set icon of AlertDialog

                mBuilder.setSingleChoiceItems(listheight, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        if (i == 0)
                        {
                            mmtext6.setText(">4.2");
                            dialog.dismiss();
                        }
                        if (i == 1)
                        {
                            mmtext6.setText("<=4.2");
                            dialog.dismiss();
                        }
                        if (i == 2)
                        {
                            mmtext6.setText("Any Type");
                            dialog.dismiss();
                        }




                    }
                }); mBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                //Show Alert Dialog
                AlertDialog mDialog=mBuilder.create();
                mDialog.show();
            }
        });



    }


}
