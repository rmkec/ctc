package com.example.ctc;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ctc.Response.CiriaResponse;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class Walls extends AppCompatActivity {

    String[] listItems, listelementt;
    Button mBtn, mbtn5, mcal, mclear, vresult;
    TextView mTextView, pmax, mtextview5;
    EditText formheight, pourheight, rate, planarea, density;


    //EditText wallrop1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walls);
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        mBtn = findViewById(R.id.button1);
        mbtn5 = findViewById(R.id.buttonelementt);

        mcal = findViewById(R.id.button2);
        mclear = findViewById(R.id.button3);
        pmax = findViewById(R.id.anspmax);
        vresult = findViewById(R.id.button4);

        mTextView = findViewById(R.id.text1);
        mtextview5 = findViewById(R.id.textelementt);
        planarea = findViewById(R.id.editrate100);

        density = findViewById(R.id.editdensity);
        formheight = findViewById(R.id.editformheight10);
        pourheight = findViewById(R.id.editpourheight);
        rate = findViewById(R.id.editvolume10);
        //contemp = findViewById(R.id.editcontemp);
        //wallrop1=findViewById(R.id.editrop1);

       // Toast.makeText(getApplicationContext(), "Latitude: " + preferences.getString("Latitude", ""), Toast.LENGTH_LONG).show();

        mclear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                formheight.getText().clear();
                pourheight.getText().clear();
                planarea.getText().clear();
                density.getText().clear();
                rate.getText().clear();
                //contemp.getText().clear();
                //wallrop1.getText().clear();
            }
        });

        mcal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Toast.makeText(getApplicationContext(),"Calculate Pressed",Toast.LENGTH_LONG).show();

                if (formheight.getText().length() == 0 || planarea.getText().length() == 0 || rate.getText().length() == 0 || pourheight.getText().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Please verify the fields", Toast.LENGTH_SHORT).show();
                } else {
                    // String den= density.getText().toString();
                    final double c2 = Double.parseDouble(mTextView.getText().toString());
                    final double elementt = Double.parseDouble(mtextview5.getText().toString());
                    final double den = Double.parseDouble(density.getText().toString());
                    final double a = Double.parseDouble(formheight.getText().toString());
                    final double b = Double.parseDouble(pourheight.getText().toString());
                    final double c = Double.parseDouble(rate.getText().toString());
                    //final double d = Double.parseDouble(contemp.getText().toString());
                    final double e2 = Double.parseDouble(planarea.getText().toString());
                    //final double r=Double.parseDouble(wallrop1.getText().toString());


                    AsyncHttpClient client = new AsyncHttpClient();
                    JSONObject jsonObject = new JSONObject();
                    StringEntity entity = null;

                    CiriaData ciriaData = new CiriaData(
                            c2,
                            elementt,
                            den,
                            a,
                            b,
                            c,
                            e2
                    );
                    try {
                        entity = new StringEntity("{}");
                        jsonObject.put("jobCode",preferences.getInt("Jobid",0));
                        jsonObject.put("userId", preferences.getInt("userID", 0));
                        jsonObject.put("latitude", preferences.getString("Latitude", null));
                        jsonObject.put("longtitude", preferences.getString("Longitude", null));
                        jsonObject.put("elementt", ciriaData.getElementt());
                        jsonObject.put("c2", ciriaData.getC2());
                        jsonObject.put("density", ciriaData.getDensity());
                        jsonObject.put("verticalFormHeight", ciriaData.getVerticalFormHeight());
                        jsonObject.put("verticalPourHeight", ciriaData.getVerticalPourHeight());
                        jsonObject.put("volume", ciriaData.getVolume());
                        jsonObject.put("planArea", ciriaData.getPlanArea());
                        entity = new StringEntity(jsonObject.toString());
                        Toast.makeText(Walls.this, "Result calculated successfully", Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                    client.post(Walls.this, "http://rmkeclandt-env.miurmysbmy.us-east-2.elasticbeanstalk.com/ciria", entity, "application/json", new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            try {
                                Log.d("Demo Success: ", new JSONObject(new String(responseBody, "UTF-8")).toString());
                                JSONObject jsonObject = new JSONObject(new String(responseBody, "UTF-8"));
                                CiriaResponse ciriaResponse = new CiriaResponse(jsonObject.getDouble("pmax"));
                                Log.d("Demo pMax: ", ciriaResponse.toString());
                                pmax.setText(Double.toString(ciriaResponse.getPmax()));
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            try {
                                JSONObject jsonObject = new JSONObject(new String(responseBody, "UTF-8"));
                                Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });


        mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listItems = new String[]{"OPC,RHPC or SRPC without admixtures\n", "OPC,RHPC or SRPC with any admixtures,except a retarder\n",
                        "OPC,RHPC or SRPC with a reterder\n", "LHBFC,PBFC,PPFAC or blends containing 70%ggbfs or 40%pfa without admixtures\n"
                        , "LHBFC,PBFC,PPFAC or blends containing lesd than 70% ggbfs or 40% pfa with any admixtures,except a retarder\n",
                        "LHPBC,PBFC,PPFAC or blends containing 70% ggbfs or 40% pfs with a retarder\n", "Blends containing " +
                        "more than 70% ggbfs or 40%pfa\n"};

                AlertDialog.Builder mBuilder = new AlertDialog.Builder(Walls.this);
                mBuilder.setTitle("Choose an item");//set title of AlertDialog
                mBuilder.setIcon(R.drawable.icon);//Set icon of AlertDialog
                mBuilder.setSingleChoiceItems(listItems, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        if (i == 0) {
                            mTextView.setText("0.3");
                            dialog.dismiss();
                        } else if (i == 1) {
                            mTextView.setText("0.3");
                            dialog.dismiss();
                        } else if (i == 2) {
                            mTextView.setText("0.45");
                            dialog.dismiss();
                        } else if (i == 3) {
                            mTextView.setText("0.45");
                            dialog.dismiss();
                        } else if (i == 4) {
                            mTextView.setText("0.45");
                            dialog.dismiss();
                        } else if (i == 5) {
                            mTextView.setText("0.6");
                            dialog.dismiss();
                        } else if (i == 6) {
                            mTextView.setText("0.6");
                            dialog.dismiss();
                        }
                    }
                });
                mBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                //Show Alert Dialog
                AlertDialog mDialog = mBuilder.create();
                mDialog.show();
            }
        });
        mbtn5.setOnClickListener(new View.OnClickListener() {
                                     @Override
                                     public void onClick(View v) {
                                         listelementt = new String[]{"\n\nWALLS\n\n", "\n\nCOLUMNS\n\n"};
                                         AlertDialog.Builder mBuilder = new AlertDialog.Builder(Walls.this);
                                         mBuilder.setTitle("C1");
                                         mBuilder.setIcon(R.drawable.icon);

                                         mBuilder.setSingleChoiceItems(listelementt, -1, new DialogInterface.OnClickListener() {
                                                     @Override
                                                     public void onClick(DialogInterface dialog, int i) {
                                                         if (i == 0) {
                                                             mtextview5.setText("1.0");
                                                             dialog.dismiss();
                                                         }

                                                         if (i == 1) {
                                                             mtextview5.setText("1.5");
                                                             dialog.dismiss();
                                                         }
                                                     }
                                                 }
                                         );
                                         mBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                                             @Override
                                             public void onClick(DialogInterface dialog, int which) {

                                             }
                                         });
                                         //Show Alert Dialog
                                         AlertDialog mDialog = mBuilder.create();
                                         mDialog.show();

                                     }
                                 }
        );

    }
}