package com.example.ctc;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.ctc.Beans.UserData;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class sign_in extends AppCompatActivity implements LocationListener,OnItemSelectedListener  {
    EditText username, password;
    Button Login;
    Spinner role;
    String[] listrole;
    int roleID;
    double latitude, longitude;

    LocationManager locationManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Login = (Button) findViewById(R.id.buttonlogin1);
        username = (EditText) findViewById(R.id.edituser1);
        password = (EditText) findViewById(R.id.editpassword1);
        role = (Spinner) findViewById(R.id.txtrole2);
        getLocation();
        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
        }
        role.setOnItemSelectedListener((OnItemSelectedListener)this);
        List<String> categories=new ArrayList<String>();
        categories.add("Admin");
        categories.add("User");
        categories.add("Manager");
        categories.add("Employee");

        ArrayAdapter<String> dataAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        role.setAdapter(dataAdapter);





        Login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (username.getText().length() == 0 || password.getText().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Please Enter the fields", Toast.LENGTH_SHORT).show();
                } else {
                    final String UserName = username.getText().toString();
                    final String Password = password.getText().toString();
                    final int Role = roleID;

                    AsyncHttpClient client = new AsyncHttpClient();
                    JSONObject jsonObject = new JSONObject();
                    StringEntity entity = null;
                    UserData userData = new UserData(UserName, Password, Role);
                    try {
                        entity = new StringEntity("{}");
                        jsonObject.put("UserName", userData.getUserName());
                        jsonObject.put("Password", userData.getPassword());
                        jsonObject.put("Role", userData.getRole());
                        entity = new StringEntity(jsonObject.toString());
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    client.post(sign_in.this, "http://rmkeclandt-env.miurmysbmy.us-east-2.elasticbeanstalk.com/userlogin", entity, "application/json", new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            try {
                                Log.d("demo Success", new JSONObject(new String(responseBody, "UTF-8")).toString());
                                JSONObject jsonObject = new JSONObject(new String(responseBody, "UTF-8"));
                                validate(jsonObject.getInt("userID"), jsonObject.getInt("roleID"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            try {
                                JSONObject jsonObject = new JSONObject(new String(responseBody, "UTF-8"));
                                Toast.makeText(getApplicationContext(), jsonObject.getString("details"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });
    }



    public void validate(int _userID, int _roleID) {
        if (_userID != 0 && (_roleID == 1) || (_roleID == 2) || (_roleID == 3) || (_roleID == 4)) {
            Intent intent = new Intent(sign_in.this, JobDetails.class);
            startActivity(intent);
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("userID", _userID);
            editor.putInt("roledID", _roleID);
            editor.commit();
        }


    }

    public void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        Toast.makeText(sign_in.this, "Location Found", Toast.LENGTH_SHORT).show();
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("Latitude", String.valueOf(latitude));
        editor.putString("Longitude", String.valueOf(longitude));
        editor.commit();


    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {
        Toast.makeText(sign_in.this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(sign_in.this);

// Setting Dialog Title
        alertDialog.setTitle("GPS is settings");
// Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");
// On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

// Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item=String.valueOf(parent.getItemIdAtPosition(position));
        Toast.makeText(parent.getContext(),"Selected: "+item,Toast.LENGTH_LONG).show();

    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        //TODO Auto-generated methos stub
    }
}