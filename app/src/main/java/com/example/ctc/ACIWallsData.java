package com.example.ctc;


public class ACIWallsData {
    String elementType;
    String cementType;
    String internalVibration;
    String slag;
    String flyash;
    String retarders;
    String slump;
    String wallheight;
    double den;
    double volume;

    double verticalformheight;
    double planareas;
   // double temperatureee;
    //double wallrop;

    public ACIWallsData(String elementType, String cementType, String internalVibration, String slag,
                        String flyash,
                        String retarders, String slump, String wallheight, double den, double volume,
                        double verticalformheight, double planareas)
    {
        super();
        this.elementType=elementType;
        this.cementType=cementType;
        this.internalVibration=internalVibration;
        this.slag=slag;
        this.flyash=flyash;
        this.retarders=retarders;
        this.slump=slump;
        this.wallheight=wallheight;
        this.den=den;
        this.volume=volume;
        this.verticalformheight=verticalformheight;
        this.planareas=planareas;
        //this.temperatureee=temperatureee;
        //this.wallrop=wallrop;

    }




    public String getElementType() {
        return elementType;
    }

    public void setElementType(String elementType) {
        this.elementType = elementType;
    }

    public String getCementType() {
        return cementType;
    }

    public void setCementType(String cementType) {
        this.cementType = cementType;
    }

    public String getInternalVibration() {
        return internalVibration;
    }

    public void setInternalVibration(String internalVibration) {
        this.internalVibration = internalVibration;
    }

    public String getSlag() {
        return slag;
    }


    public void setSlag(String slag) {
        this.slag = slag;
    }

    public String getFlyash() {
        return flyash;
    }

    public void setFlyash(String flyash) {
        this.flyash = flyash;
    }

    public String getRetarders() {
        return retarders;
    }

    public void setRetarders(String retarders) {
        this.retarders = retarders;
    }

    public String getSlump() {
        return slump;
    }

    public void setSlump(String slump) {
        this.slump = slump;
    }

    public String getWallheight() {
        return wallheight;
    }

    public void setWallheight(String wallheight) {
        this.wallheight = wallheight;
    }

    public double getDen() {
        return den;
    }

    public void setDen(double den) {
        this.den = den;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public double getVerticalformheight() {
        return verticalformheight;
    }

    public void setVerticalformheight(double verticalformheight) {
        this.verticalformheight = verticalformheight;
    }

    public double getPlanareas() {
        return planareas;
    }

    public void setPlanareas(double planareas) {
        this.planareas = planareas;
    }



   // public double getWallrop()  {  return wallrop;}

    //public void setWallrop(double wallrop) { this.wallrop=wallrop;}

}
