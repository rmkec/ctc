package com.example.ctc.Adapter;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ctc.Item.CiriaItems;
import com.example.ctc.R;

import java.util.List;


public class CiriaAdapter1 extends AppCompatActivity {
    TextView ac, bc, cc, dc, ec, fc, gc, hc, ic, jc, kc, lc, mc,nc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_ciria1);

        Intent intent1 =getIntent();
        double ELEMENT = intent1.getDoubleExtra("elementt",0);

        double C2 = intent1.getDoubleExtra("c2",0);
        double DENSITY = intent1.getDoubleExtra("density",0);
        double FORMHEIGHT = intent1.getDoubleExtra("verticalFormHeight",0);
        double POURHEIGHT = intent1.getDoubleExtra("verticalPourHeight",0);
        double VOLUME = intent1.getDoubleExtra("volume",0);
        double PLANAREA = intent1.getDoubleExtra("planArea",0);
        double TEMPERATURE = intent1.getDoubleExtra("temperature",0);
        double PMAX = intent1.getDoubleExtra("pmax",0);
        int JOBCODE  = intent1.getIntExtra("jobcode",0);
        String USERNAME = intent1.getStringExtra("userName");
        String JOBNAME = intent1.getStringExtra("jobName");
        String JOBLOCATION = intent1.getStringExtra("jobLocation");
        String CDATE=intent1.getStringExtra("Cdate_time");


        ac=findViewById(R.id.resultc1a);
        bc=findViewById(R.id.resultc2a);
        cc=findViewById(R.id.resultdena);
        dc=findViewById(R.id.resultfha);
        ec=findViewById(R.id.resultpha);
        fc=findViewById(R.id.resultvola);
        gc=findViewById(R.id.resultpaa);
        hc=findViewById(R.id.resulttempa);
        ic=findViewById(R.id.resultpmxa);
        jc=findViewById(R.id.resultcjobca);
        kc=findViewById(R.id.resultcuna);
        lc=findViewById(R.id.resultcjobna);
        mc=findViewById(R.id.resultcjobla);
        nc=findViewById(R.id.datec);

        ac.setText("ELEMENT:"+ELEMENT);
        bc.setText("C2:"+C2);
        cc.setText("DENSITY:"+DENSITY);
        dc.setText("VERTICALFORMHEIGHT:"+FORMHEIGHT);
        ec.setText("VERTICALPOURHEIGHT:"+POURHEIGHT);
        fc.setText("VOLUME:"+VOLUME);
        gc.setText("PLANAREA:"+PLANAREA);
        hc.setText("TEMPERATURE:"+TEMPERATURE);
        ic.setText("PMAX:"+PMAX);
        jc.setText("JOBCODE:"+JOBCODE);
        kc.setText("USERNAME:"+USERNAME);
        lc.setText("JOBNAME:"+JOBNAME);
        mc.setText("JOBLOCATION:"+JOBLOCATION);
        nc.setText("DATE&TIME:"+CDATE);





    }
}
