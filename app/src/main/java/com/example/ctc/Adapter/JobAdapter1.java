package com.example.ctc.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ctc.Item.JobItem;
import com.example.ctc.R;

import java.util.List;


public class JobAdapter1 extends AppCompatActivity {
    TextView aj, bj, cj, dj, ej, fj, gj, hj, ij;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.job_all_result);
            final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            Intent intent=getIntent();
            int JOB_ID=intent.getIntExtra("jobID",0);
            String JOB_NAME=intent.getStringExtra("jobName");
            String JOB_CODE=intent.getStringExtra("jobCode");
            String JOB_LOCATION=intent.getStringExtra("jobLocation");
            String CLUSTER=intent.getStringExtra("cluster");
            String STRUCTURE=intent.getStringExtra("structureName");
            String ELEMENT=intent.getStringExtra("jobName");
            int DURATION=intent.getIntExtra("duration",0);
            String DATE=intent.getStringExtra("date_time");


            aj=findViewById(R.id.jccurr);
            bj=findViewById(R.id.jncurr);
            cj=findViewById(R.id.joblcurr);
            dj=findViewById(R.id.clustercurr);
            ej=findViewById(R.id.uncurr);
            fj=findViewById(R.id.datecurr);
            gj=findViewById(R.id.jscurr);
            hj=findViewById(R.id.jecurr);
            ij=findViewById(R.id.jdurcurr);


            aj.setText("JOB CODE :"+JOB_CODE);
            bj.setText("JOB NAME :"+JOB_NAME);
            cj.setText("JOB LOCATION :"+JOB_LOCATION);
            dj.setText("CLUSTER :"+CLUSTER);
            ej.setText("USER NAME :"+preferences.getString("UserName",""));
            fj.setText("DATE AND TIME :"+DATE);
            gj.setText("STRUCTURE NAME :"+STRUCTURE);
            hj.setText("ELEMENT :"+ELEMENT);
            ij.setText("DURATION :"+DURATION);




        }
    }


