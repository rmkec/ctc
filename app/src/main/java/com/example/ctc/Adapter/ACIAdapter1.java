package com.example.ctc.Adapter;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ctc.Item.CiriaItems;
import com.example.ctc.R;

import java.util.List;


public class ACIAdapter1 extends AppCompatActivity {
    TextView ac1, bc1, cc1, dc1, ec1, fc1, gc1, hc1, ic1, jc1, kc1, lc1, mc1, nc1, oc1, pc1, qc1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_aci1);

        Intent intent1 =getIntent();
        String IVD = intent1.getStringExtra("internalVibration");
        String ELEMENT = intent1.getStringExtra("elementType");
        String SLUMP = intent1.getStringExtra("slump");
        double DENSITY = intent1.getDoubleExtra("den",0);
        double FORMHEIGHT = intent1.getDoubleExtra("verticalformheight",0);
        double VOLUME = intent1.getDoubleExtra("volume",0);
        double PLANAREA = intent1.getDoubleExtra("planareas",0);
        double TEMPERATURE = intent1.getDoubleExtra("temperatureee",0);
        double PMAX = intent1.getDoubleExtra("pmax",0);
        int JOBCODE = intent1.getIntExtra("jobCode",0);
        String USERNAME = intent1.getStringExtra("userName");
        String JOBNAME = intent1.getStringExtra("jobName");
        String JOBLOCATION = intent1.getStringExtra("jobLocation");
        String SLAG = intent1.getStringExtra("slag");
        String FLYASH = intent1.getStringExtra("flyash");
        String RETARDERS = intent1.getStringExtra("retarders");
        String WALLHEIGHT = intent1.getStringExtra("wallheight");

        ac1=findViewById(R.id.resultaivd);
        bc1=findViewById(R.id.resultaele);
        cc1=findViewById(R.id.resultaslump);
        dc1=findViewById(R.id.resultaden);
        ec1=findViewById(R.id.resultafh);
        fc1=findViewById(R.id.resultavol);
        gc1=findViewById(R.id.resultapa);
        hc1=findViewById(R.id.resultatemp);
        ic1=findViewById(R.id.resultapmx);
        jc1=findViewById(R.id.resultajobc);
        kc1=findViewById(R.id.resultacun);
        lc1=findViewById(R.id.resultajobn);
        mc1=findViewById(R.id.resultajobl);
        nc1=findViewById(R.id.resultajobslag);
        oc1=findViewById(R.id.resultajobflyash);
        pc1=findViewById(R.id.resultajobretarders);
        qc1=findViewById(R.id.resultajobwallheight);

        ac1.setText("Internal Vibration Depth:"+IVD);
        bc1.setText("Element:"+ELEMENT);
        cc1.setText("Slump:"+SLUMP);
        dc1.setText("Density:"+DENSITY);
        ec1.setText("Vertical Form Height :"+FORMHEIGHT);
        fc1.setText("Volume:"+VOLUME);
        gc1.setText("Plan Area:"+PLANAREA);
        hc1.setText("Temperature:"+TEMPERATURE);
        ic1.setText("PMAX:"+PMAX);
        jc1.setText("Jobcode:"+JOBCODE);
        kc1.setText("Username:"+USERNAME);
        lc1.setText("Jobname:"+JOBNAME);
        mc1.setText("JobLocation:"+JOBLOCATION);
        nc1.setText("Slag:"+SLAG);
        oc1.setText("Fly Ash:"+FLYASH);
        pc1.setText("Reatarders:"+RETARDERS);
        qc1.setText("Wall Height:"+WALLHEIGHT);





    }
}
