package com.example.ctc.Adapter;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ctc.Item.CiriaItems;
import com.example.ctc.R;

import java.util.List;

import com.example.ctc.Item.JobItem;


public class CiriaAdapter extends RecyclerView.Adapter<CiriaAdapter.CiriaViewHolder> {

    Context mcontext;
    List<CiriaItems> ciriaresponse;
    CiriaAdapter.OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(CiriaAdapter.OnItemClickListener listener) {
        mListener = (OnItemClickListener) listener;
    }

    public CiriaAdapter(Context mcontext, List<CiriaItems> mdata) {
        this.mcontext = mcontext;
        this.ciriaresponse = mdata;
    }


    @NonNull
    @Override
    public CiriaViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View layout;
        layout = LayoutInflater.from(mcontext).inflate(R.layout.activity_result_ciria, viewGroup, false);
        return new CiriaViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull final CiriaViewHolder ciriaViewHolder, final int position) {
        ciriaViewHolder.jobReportName1.setText(ciriaresponse.get(position).getJobName1() + " - " + ciriaresponse.get(position).getJobCode1()+","+ciriaresponse.get(position).getUserName1());
        ciriaViewHolder.jobReportDetails1.setText(String.valueOf(ciriaresponse.get(position).getCreatedON1()));
        ciriaViewHolder.jobReportDate1.setText(String.valueOf(ciriaresponse.get(position).getJobLocation1()));

        ciriaViewHolder.ciria1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent detailIntent = new Intent(mcontext, CiriaAdapter1.class);
                detailIntent.putExtra("elementt", ciriaresponse.get(position).getElementt1());
                detailIntent.putExtra("c2", ciriaresponse.get(position).getC21());
                detailIntent.putExtra("density", ciriaresponse.get(position).getDensity1());
                detailIntent.putExtra("verticalFormHeight", ciriaresponse.get(position).getVerticalFormHeight1());
                detailIntent.putExtra("verticalPourHeight", ciriaresponse.get(position).getVerticalPourHeight1());
                detailIntent.putExtra("volume", ciriaresponse.get(position).getVolume1());
                detailIntent.putExtra("planArea", ciriaresponse.get(position).getPlanArea1());
                detailIntent.putExtra("temperature", ciriaresponse.get(position).getTemperature1());
                detailIntent.putExtra("pmax", ciriaresponse.get(position).getPmax1());
                detailIntent.putExtra("jobcode", ciriaresponse.get(position).getJobCode1());
                detailIntent.putExtra("userName", ciriaresponse.get(position).getUserName1());
                detailIntent.putExtra("jobName", ciriaresponse.get(position).getJobName1());
                detailIntent.putExtra("jobLocation", ciriaresponse.get(position).getJobLocation1());
                detailIntent.putExtra("Cdate_time",ciriaresponse.get(position).getCreatedON1());
                mcontext.startActivity(detailIntent);
            }
        });




    }

    @Override
    public int getItemCount() {

        return ciriaresponse.size();
    }


    public static class CiriaViewHolder extends RecyclerView.ViewHolder {


        static TextView jobReportName1, jobReportDate1, jobReportDetails1;
        CardView ciria1;

        public CiriaViewHolder(@NonNull View itemView) {
            super(itemView);
            ciria1 = itemView.findViewById(R.id.ciria1);
            jobReportName1 = itemView.findViewById(R.id.jobReportName1);
            jobReportDate1 = itemView.findViewById(R.id.jobReportDate1);
            jobReportDetails1 = itemView.findViewById(R.id.jobReportDetails1);
        }
    }


}
