package com.example.ctc.Adapter;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import com.example.ctc.Item.AciItem;
import com.example.ctc.R;

import java.util.ArrayList;
import java.util.List;

public class AciAdapter extends  RecyclerView.Adapter<AciAdapter.AciViewHolder> implements Filterable {


    Context mcontext2;
    List<AciItem> mdata2;
    AciAdapter.OnItemClickListener mListener;
    List<AciItem> exampleListFull;
    @Override
    public Filter getFilter() {
        return exampleFilter;
    }
    private Filter exampleFilter= new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<AciItem> filterList= new ArrayList<>();
            if(constraint==null || constraint.length()==0)
            {
                filterList.addAll(exampleListFull);

            }
            else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for(AciItem item: exampleListFull){
                    if(item.getUsername2().toLowerCase().contains(filterPattern)){
                        filterList.add(item);
                    }
                }
            }
            FilterResults results= new FilterResults();
            results.values=filterList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mdata2.clear();
            mdata2.addAll((List) results.values);
            notifyDataSetChanged();




        }
    };

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(AciAdapter.OnItemClickListener listener) {
        mListener = (OnItemClickListener) listener;
    }


    public AciAdapter(Context mcontext2, List<AciItem> mdata2) {
        this.mcontext2 = mcontext2;
        this.mdata2 = mdata2;
    }



    @NonNull
    @Override
    public AciAdapter.AciViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View layout;
        layout = LayoutInflater.from(mcontext2).inflate(R.layout.activity_result_aci, viewGroup, false);
        return new AciAdapter.AciViewHolder(layout);
    }


    @Override
    public void onBindViewHolder(@NonNull AciAdapter.AciViewHolder jobsViewHolder, final int position) {
        AciViewHolder.jobcode2.setText(mdata2.get(position).getAciID() + " - " + mdata2.get(position).getJobCode2());
        AciViewHolder.element2.setText(mdata2.get(position).getCementType2());
        AciViewHolder.pmax2.setText( mdata2.get(position).getElementType2());


        AciViewHolder.aci1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent detailIntent = new Intent(mcontext2, ACIAdapter1.class);
                detailIntent.putExtra("internalVibration", mdata2.get(position).getInternalVibration2());
                detailIntent.putExtra("elementType", mdata2.get(position).getElementType2());
                detailIntent.putExtra("slump", mdata2.get(position).getSlump2());
                detailIntent.putExtra("den", mdata2.get(position).getDen2());
                detailIntent.putExtra("verticalformheight", mdata2.get(position).getVerticalformheight2());
                detailIntent.putExtra("volume", mdata2.get(position).getVolume2());
                detailIntent.putExtra("planareas", mdata2.get(position).getPlanareas2());
                detailIntent.putExtra("temperatureee", mdata2.get(position).getTemperatureee2());
                detailIntent.putExtra("pmax", mdata2.get(position).getPmax2());
                detailIntent.putExtra("jobCode", mdata2.get(position).getJobCode2());
                detailIntent.putExtra("userName", mdata2.get(position).getUsername2());
                detailIntent.putExtra("jobName", mdata2.get(position).getJobname2());
                detailIntent.putExtra("jobLocation", mdata2.get(position).getJoblocation2());
                detailIntent.putExtra("slag", mdata2.get(position).getSlag2());
                detailIntent.putExtra("flyash", mdata2.get(position).getFlyash2());
                detailIntent.putExtra("retarders", mdata2.get(position).getRetarders2());
                detailIntent.putExtra("wallheight", mdata2.get(position).getWallheight2());
                mcontext2.startActivity(detailIntent);
            }
        });



    }

    @Override
    public int getItemCount() {
        return mdata2.size();
    }

    public static class AciViewHolder extends RecyclerView.ViewHolder {


        static TextView jobcode2, element2, pmax2;
        static CardView aci1;

        public AciViewHolder(@NonNull View itemView) {
            super(itemView);

            jobcode2 = itemView.findViewById(R.id.jobReportName2);
            element2 = itemView.findViewById(R.id.jobReportDate2);
            pmax2 = itemView.findViewById(R.id.jobReportDetails2);
            aci1=itemView.findViewById(R.id.aci1);
        }
    }

}
