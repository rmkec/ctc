package com.example.ctc.Adapter;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ctc.Item.JobItem;
import com.example.ctc.R;

import java.util.List;





public class JobsAdapter extends RecyclerView.Adapter<JobsAdapter.JobsViewHolder> {

    Context mcontext1;
    List<JobItem> mdata;
    OnItemClickListener mListener;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener)
    {
        mListener=listener;
    }
    public JobsAdapter(Context mcontext1, List<JobItem> mdata) {
        this.mcontext1 = mcontext1;
        this.mdata = mdata;
    }

    @NonNull
    @Override
    public JobsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View layout;
        layout = LayoutInflater.from(mcontext1).inflate(R.layout.job_items, viewGroup, false);
        return new JobsViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull JobsViewHolder jobsViewHolder, final int position) {

        jobsViewHolder.jobName.setText(mdata.get(position).getJobCode() + " - " + mdata.get(position).getJobName()+","+mdata.get(position).getUsername());
        jobsViewHolder.jobDetails.setText(mdata.get(position).getDatejob());
        jobsViewHolder.jobDate.setText(mdata.get(position).getJobLocation());
        jobsViewHolder.job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent detailIntent=new Intent(mcontext1,JobAdapter1.class);
                detailIntent.putExtra("jobID",mdata.get(position).getJobID());
                detailIntent.putExtra("duration",mdata.get(position).getDuration());
                detailIntent.putExtra("jobCode",mdata.get(position).getJobCode());
                detailIntent.putExtra("jobName",mdata.get(position).getJobName());
                detailIntent.putExtra("jobLocation",mdata.get(position).getJobLocation());
                detailIntent.putExtra("cluster",mdata.get(position).getCluster());
                detailIntent.putExtra("structureName",mdata.get(position).getStructureName());
                detailIntent.putExtra("elementName",mdata.get(position).getElementName());
                detailIntent.putExtra("date_time",mdata.get(position).getDatejob());
                mcontext1.startActivity(detailIntent);
            }
        });



    }

    @Override
    public int getItemCount() {
        return mdata.size();
    }

    public class JobsViewHolder extends RecyclerView.ViewHolder {


        TextView jobName, jobDate, jobDetails;
        CardView job;

        public JobsViewHolder(@NonNull View itemView) {

            super(itemView);
            jobName = itemView.findViewById(R.id.jobReportName);
            jobDate = itemView.findViewById(R.id.jobReportDate);
            jobDetails = itemView.findViewById(R.id.jobReportDetails);
            job=itemView.findViewById(R.id.job);

        }
    }



}
