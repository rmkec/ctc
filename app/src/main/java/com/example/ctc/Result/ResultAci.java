package com.example.ctc.Result;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ctc.Item.CiriaItems;
import com.example.ctc.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class ResultAci extends AppCompatActivity {


    TextView textivd,textslump,textdensity,textvfh,textvolume,textplanarea,texttemp,textpmax,textjobcode,textusername,textjobname,textjoblocation,textslag,textflyash,textretarders,textwallheight,textelement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.current_aci);
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);


        textivd = findViewById(R.id.resultaivd);
        textslump = findViewById(R.id.resultaslump);
        textdensity = findViewById(R.id.resultaden);
        textvfh = findViewById(R.id.resultafh);
        textvolume = findViewById(R.id.resultavol);
        textplanarea = findViewById(R.id.resultapa);
        texttemp = findViewById(R.id.resultatemp);
        textpmax = findViewById(R.id.resultapmx);
        textjobcode = findViewById(R.id.resultajobc);
        textusername = findViewById(R.id.resultacun);
        textjobname = findViewById(R.id.resultajobn);
        textjoblocation = findViewById(R.id.resultajobl);
        textslag = findViewById(R.id.resultajobslag);
        textflyash = findViewById(R.id.resultajobflyash);
        textretarders = findViewById(R.id.resultajobretarders);
        textwallheight = findViewById(R.id.resultajobwallheight);
        textelement = findViewById(R.id.resultaele);





        textivd.setText("Internal Vibration Depth :" +preferences.getString("ivd",null));
        textslump.setText("Slump :" +preferences.getString("slump",null));
        textdensity.setText("Density :" +preferences.getString("density",null));
        textvfh.setText("Vertical Form Height :" +preferences.getString("vfh",null));
        textvolume.setText("Volume :" +preferences.getString("volume",null));
        textplanarea.setText("Plan Area :" +preferences.getString("planarea",null));
        texttemp.setText("Temperature :" +preferences.getString("temperature",null));
        textpmax.setText("ccpmax :" +preferences.getString("ccpmax",null));
        textjobcode.setText("JobCode :" +preferences.getString("JobCode",null));
        textusername.setText("UserName :" +preferences.getString("UserName",""));
        textjobname.setText("JobName :" +preferences.getString("JobName",null));
        textjoblocation.setText("JobLocation :" +preferences.getString("JobLocation",""));
        textslag.setText("Slag :" +preferences.getString("slag",null));
        textflyash.setText("Flyash :" +preferences.getString("flyash",null));
        textretarders.setText("Retarders :" +preferences.getString("retarders",null));
        textwallheight.setText("Wall Height :" +preferences.getString("wallheight",null));
        textelement.setText("Element :" +preferences.getString("element",null));







    }
}
