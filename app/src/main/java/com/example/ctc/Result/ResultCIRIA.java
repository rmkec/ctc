package com.example.ctc.Result;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ctc.Activity.CiriaActivity;
import com.example.ctc.Item.CiriaItems;
import com.example.ctc.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class ResultCIRIA extends AppCompatActivity {

    TextView Ac,Bc,Cc;
    TextView Dc,Ec,Fc;
    TextView Gc,Hc,Ic,Jc,Kc,Lc,Mc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.current_ciria);
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        Ac = findViewById(R.id.resultc1);
        Bc = findViewById(R.id.resultc2);
        Cc = findViewById(R.id.resultden);
        Dc = findViewById(R.id.resultfh);
        Ec = findViewById(R.id.resultph);
        Fc = findViewById(R.id.resultvol);
        Gc = findViewById(R.id.resultpa);
        Hc = findViewById(R.id.resulttemp);
        Ic = findViewById(R.id.resultpmx);
        Jc = findViewById(R.id.resultcjobc);
        Kc = findViewById(R.id.resultcun);
        Lc = findViewById(R.id.resultcjobn);
        Mc = findViewById(R.id.resultcjobl);



        Ac.setText("C1 :" +preferences.getString("c1",null));
        Bc.setText("C2 :" +preferences.getString("c2",null));
        Cc.setText("Density :" +preferences.getString("Density",null));
        Dc.setText("Form Height :" +preferences.getString("Form_Height",null));
        Ec.setText("Pour Height :" +preferences.getString("Pour_Height",null));
        Fc.setText("Volume :" +preferences.getString("Volume",null));
        Gc.setText("Planarea :" +preferences.getString("Planarea",null));
        Ic.setText("Pmax :" +preferences.getString("Pmax",null));
        Hc.setText("Temperature :" +preferences.getString("Temperature",null));
        Jc.setText("JobCode :" +preferences.getString("JobCode",null));
        Kc.setText("UserName :" +preferences.getString("UserName",null));
        Lc.setText("JobName :" +preferences.getString("JobName",null));
        Mc.setText("JobLocation :" +preferences.getString("JobLocation",null));



    }
}

