package com.example.ctc.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.ctc.Beans.DashboardActivity;
import com.example.ctc.Beans.UserData;
import com.example.ctc.JobDetails;
import com.example.ctc.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class LoginActivity extends AppCompatActivity implements LocationListener {

    private EditText username, password;
    private Button loginBtn;
    private Spinner roleSpinner;
    private int roleID;
    private double latitude, longitude;
    private LocationManager locationManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getLocation();
        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
        }

        username = findViewById(R.id.edituser1);
        password = findViewById(R.id.editpassword1);
        loginBtn = findViewById(R.id.buttonlogin1);
        roleSpinner = findViewById(R.id.txtrole2);

        ArrayAdapter<String> roleAdapter = new ArrayAdapter<String>(LoginActivity.this, R.layout.support_simple_spinner_dropdown_item, getResources().getStringArray(R.array.roles));
        roleAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        roleSpinner.setAdapter(roleAdapter);

        roleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String text = roleSpinner.getSelectedItem().toString();
                switch (text) {
                    case "Admin":
                        roleID = 1;
                        break;
                    case "Manager":
                        roleID = 2;
                        break;
                    case "Employee":
                        roleID = 3;
                        break;
                    case "User":
                        roleID = 4;
                        break;
                    default:
                        roleID = 0;
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(LoginActivity.this, "Please Select Role", Toast.LENGTH_SHORT).show();
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (username.getText().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Please Enter User Name", Toast.LENGTH_SHORT).show();
                } else if (password.getText().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Please Enter Password", Toast.LENGTH_SHORT).show();
                } else if (roleID == 0) {
                    Toast.makeText(getApplicationContext(), "Please Choose Role", Toast.LENGTH_SHORT).show();
                } else {
                    final String UserName = username.getText().toString();
                    final String Password = password.getText().toString();
                    final int Role = roleID;

                    AsyncHttpClient client = new AsyncHttpClient();
                    JSONObject jsonObject = new JSONObject();
                    StringEntity entity = null;
                    UserData userData = new UserData(UserName, Password, Role);
                    SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("UserName", userData.getUserName());
                    editor.commit();
                    try {
                        entity = new StringEntity("{}");
                        jsonObject.put("UserName", userData.getUserName());
                        jsonObject.put("Password", userData.getPassword());
                        jsonObject.put("Role", userData.getRole());
                        entity = new StringEntity(jsonObject.toString());
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    client.post(LoginActivity.this, "http://rmkeclandt-env.miurmysbmy.us-east-2.elasticbeanstalk.com/userlogin", entity, "application/json", new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            try {
                                JSONObject jsonObject = new JSONObject(new String(responseBody, "UTF-8"));
                                validate(jsonObject.getInt("userID"), jsonObject.getInt("roleID"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            try {
                                JSONObject jsonObject = new JSONObject(new String(responseBody, "UTF-8"));
                                Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });
    }

    public void validate(int _userID, int _roleID) {
        if (_userID != 0 && (_roleID == 1) || (_roleID == 2) || (_roleID == 3) || (_roleID == 4)) {
            Intent intent = new Intent(LoginActivity.this, JobDetails.class);
            startActivity(intent);
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("userID", _userID);
            editor.putInt("roledID", _roleID);
            editor.commit();
        }
    }

    public void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, this);




        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        Toast.makeText(LoginActivity.this, "Location Found", Toast.LENGTH_SHORT).show();
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("Latitude", String.valueOf(latitude));
        editor.putString("Longitude", String.valueOf(longitude));
        editor.commit();


    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {
        Toast.makeText(LoginActivity.this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(LoginActivity.this);
        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");
        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");
        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });
        Dialog dialog=alertDialog.create();
        dialog.show();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        // Showing Alert Message
        //alertDialog.show();
        //alertDialog.setCancelable(false);
    }
}
