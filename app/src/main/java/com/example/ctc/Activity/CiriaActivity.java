package com.example.ctc.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ctc.CiriaData;
import com.example.ctc.Response.CiriaResponse;
import com.example.ctc.List.ListCiria;
import com.example.ctc.R;
import com.example.ctc.Result.ResultCIRIA;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class CiriaActivity extends AppCompatActivity {

   Button cal,clr,vr;
   Spinner rolec1,rolec2;
   EditText den,ph,fh,vol,pa;
   TextView pm;
   double C1,C2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ciria);
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        cal = findViewById(R.id.calculate);
        clr = findViewById(R.id.clear1);

        vr = findViewById(R.id.viewresult);
        rolec1=findViewById(R.id.C1);
        rolec2=findViewById(R.id.C2);
        den= findViewById(R.id.density);
        ph= findViewById(R.id.pourheight);
        fh= findViewById(R.id.formheight);
        vol= findViewById(R.id.volume);
        pa= findViewById(R.id.planarea);
        pm=findViewById(R.id.pmax);





        // Toast.makeText(getApplicationContext(), "Latitude: " + preferences.getString("Latitude", ""), Toast.LENGTH_LONG).show();

        clr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                den.getText().clear();
                fh.getText().clear();
                ph.getText().clear();
                vol.getText().clear();
                pa.getText().clear();


            }
        });
        ArrayAdapter<String> c1 = new ArrayAdapter<String>(CiriaActivity.this, R.layout.support_simple_spinner_dropdown_item, getResources().getStringArray(R.array.elementciria));
        c1.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        rolec1.setAdapter(c1);

        rolec1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String text = rolec1.getSelectedItem().toString();
                switch (text) {
                    case "Walls":
                        C1 =1.0;
                        break;
                    case "Columns":
                        C1 =1.5;
                        break;


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(CiriaActivity.this, "Please Select C1", Toast.LENGTH_SHORT).show();
            }
        });
        ArrayAdapter<String> c2 = new ArrayAdapter<String>(CiriaActivity.this, R.layout.support_simple_spinner_dropdown_item, getResources().getStringArray(R.array.c2ciria));
        c2.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        rolec2.setAdapter(c2);

        rolec2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String text = rolec2.getSelectedItem().toString();
                switch (text) {
                    case "OPC,RHPC or SRPC without admixtures":
                        C2 = 0.3;
                        break;
                    case "OPC,RHPC or SRPC with any admixtures,except a retarder":
                        C2 = 0.3;
                        break;
                    case "OPC,RHPC or SRPC with a reterder":
                        C2 = 0.45;
                        break;
                    case "LHBFC,PBFC,PPFAC or blends containing 70%ggbfs or 40%pfa without admixtures":
                        C2 = 0.45;
                        break;
                    case "LHBFC,PBFC,PPFAC or blends containing lesd than 70% ggbfs or 40% pfa with any admixtures,except a retarder":
                        C2 = 0.45;
                        break;
                    case "LHPBC,PBFC,PPFAC or blends containing 70% ggbfs or 40% pfs with a retarder":
                        C2 = 0.6;
                        break;
                    case "Blends containing more than 70% ggbfs or 40%pfa":
                        C2 = 0.6;
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(CiriaActivity.this, "Please Select C2", Toast.LENGTH_SHORT).show();
            }
        });



        cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Toast.makeText(getApplicationContext(),"Calculate Pressed",Toast.LENGTH_LONG).show();

                if (fh.getText().length() == 0 || pa.getText().length() == 0  || den.getText().length()==0 || vol.getText().length() == 0 || ph.getText().length() == 0){
                    Toast.makeText(getApplicationContext(), "Please verify the fields", Toast.LENGTH_SHORT).show();
                } else {
                    // String den= density.getText().toString();
                    final double c2 = C2;
                    final double elementt = C1;
                    final double Den =Double.parseDouble(den.getText().toString());
                    final double a = Double.parseDouble(fh.getText().toString());
                    final double b = Double.parseDouble(ph.getText().toString());
                    final double c = Double.parseDouble(vol.getText().toString());
                    //final double d = Double.parseDouble(contemp.getText().toString());
                    final double e2 = Double.parseDouble(pa.getText().toString());
                    //final double r=Double.parseDouble(wallrop1.getText().toString());


                    AsyncHttpClient client = new AsyncHttpClient();
                    JSONObject jsonObject = new JSONObject();
                    StringEntity entity = null;

                    CiriaData ciriaData = new CiriaData(
                            c2,
                            elementt,
                            Den,
                            a,
                            b,
                            c,
                            e2
                    );

                    SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("c1",String.valueOf(ciriaData.getElementt()));
                    editor.putString("c2",String.valueOf(ciriaData.getC2()));
                    editor.putString("Density",String.valueOf(ciriaData.getDensity()));
                    editor.putString("Form_Height",String.valueOf(ciriaData.getVerticalFormHeight()));
                    editor.putString("Pour_Height",String.valueOf(ciriaData.getVerticalPourHeight()));
                    editor.putString("Volume",String.valueOf(ciriaData.getVolume()));
                    editor.putString("Planarea",String.valueOf(ciriaData.getPlanArea()));

                    editor.commit();


                    try {
                        entity = new StringEntity("{}");
                        jsonObject.put("jobID",preferences.getInt("Jobid",1));
                        jsonObject.put("userId", preferences.getInt("userID", 0));
                        jsonObject.put("latitude", preferences.getString("Latitude", null));
                        jsonObject.put("longtitude", preferences.getString("Longitude", null));
                        jsonObject.put("elementt", ciriaData.getElementt());
                        jsonObject.put("c2", ciriaData.getC2());
                        jsonObject.put("density", ciriaData.getDensity());
                        jsonObject.put("verticalFormHeight", ciriaData.getVerticalFormHeight());
                        jsonObject.put("verticalPourHeight", ciriaData.getVerticalPourHeight());
                        jsonObject.put("volume", ciriaData.getVolume());
                        jsonObject.put("planArea", ciriaData.getPlanArea());
                        entity = new StringEntity(jsonObject.toString());
                        Toast.makeText(CiriaActivity.this, "Result calculated successfully", Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                    client.post(CiriaActivity.this, "http://rmkeclandt-env.miurmysbmy.us-east-2.elasticbeanstalk.com/ciria", entity, "application/json", new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            try {
                                Log.d("Demo Success: ", new JSONObject(new String(responseBody, "UTF-8")).toString());
                                JSONObject jsonObject = new JSONObject(new String(responseBody, "UTF-8"));
                                CiriaResponse ciriaResponse = new CiriaResponse(jsonObject.getDouble("pmax"));
                                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                SharedPreferences.Editor editor = settings.edit();
                                editor.putInt("Ciria_id", jsonObject.getInt("ciriaID"));
                                editor.putString("Temperature",String.valueOf(jsonObject.getInt("temperature")));
                                editor.putString("Pmax",String.valueOf(jsonObject.getDouble("pmax")));
                                editor.commit();
                                jsonObject.getInt("temperature");
                                Log.d("Demo pMax: ", ciriaResponse.toString());
                                pm.setText(Double.toString(ciriaResponse.getPmax()));
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            try {
                                JSONObject jsonObject = new JSONObject(new String(responseBody, "UTF-8"));
                                Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });


        vr.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CiriaActivity.this, ResultCIRIA.class);
                startActivity(intent);
            }
        });




    }
}

