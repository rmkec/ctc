package com.example.ctc.Activity;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.ctc.Beans.DashboardActivity;
import com.example.ctc.R;

public class SplashActivity extends AppCompatActivity {


    private ImageView logo, logo1;
    private static int splashTimeOut = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        logo = (ImageView) findViewById(R.id.logo);
        logo1 = (ImageView) findViewById(R.id.logo1);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, splashTimeOut);
            Animation myanim = AnimationUtils.loadAnimation(this, R.anim.anim);
            logo.startAnimation(myanim);
            logo1.startAnimation(myanim);

    }


}
