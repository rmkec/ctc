package com.example.ctc.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;


import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.example.ctc.ACIWallsData;
import com.example.ctc.Response.ACIWallsResponse;
import com.example.ctc.List.ListAci;
import com.example.ctc.R;
import com.example.ctc.Result.ResultAci;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class ACIActivity extends AppCompatActivity {


   Spinner ret,ele,cement,ivd,slag,flyash,slump,wh;
   EditText denaci,poh,volaci,paaci;
   TextView ccp;
   Button cler,calc,vir;
   String retarders,element,cementaci,ivdaci,slagg,flyashh,slumpp,whh;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        setContentView(R.layout.activity_aci);

       ret=findViewById(R.id.retardersaci);
       ele=findViewById(R.id.elementaci);
       cement=findViewById(R.id.cementaci);
       ivd=findViewById(R.id.ivdaci);
       slag=findViewById(R.id.slagaci);
       slump=findViewById(R.id.slumpaci);
       flyash=findViewById(R.id.flyashaci);
       wh=findViewById(R.id.wallheightaci);
       denaci=findViewById(R.id.densityaci);
       poh=findViewById(R.id.heightaci);
       volaci=findViewById(R.id.volumeaci);
       paaci=findViewById(R.id.planareaaci);
       ccp=findViewById(R.id.ccpmax1);
       cler=findViewById(R.id.clearaci);
       calc=findViewById(R.id.calculateaci);
       vir=findViewById(R.id.viewresultaci);





        cler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                volaci.getText().clear();
                paaci.getText().clear();
                denaci.getText().clear();
                poh.getText().clear();

            }
        });


        ArrayAdapter<String> retarr = new ArrayAdapter<String>(ACIActivity.this, R.layout.support_simple_spinner_dropdown_item, getResources().getStringArray(R.array.retarders));
        retarr.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        ret.setAdapter(retarr);

        ret.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String text = ret.getSelectedItem().toString();
                switch (text) {
                    case "Yes":
                        retarders="Yes";
                        break;
                    case "No":
                        retarders="No";
                        break;


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(ACIActivity.this, "Please Select Retarders", Toast.LENGTH_SHORT).show();
            }
        });
        final ArrayAdapter<String> slumpaci = new ArrayAdapter<String>(ACIActivity.this, R.layout.support_simple_spinner_dropdown_item, getResources().getStringArray(R.array.slump));
        slumpaci.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        slump.setAdapter(slumpaci);

        slump.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String text = slump.getSelectedItem().toString();
                switch (text) {
                    case "Less Than or Equal to 175":
                        slumpp = "<=175";
                        break;
                    case "Greater Than 175":
                        slumpp = ">175";
                        break;


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(ACIActivity.this, "Please Select slump", Toast.LENGTH_SHORT).show();
            }
        });
        ArrayAdapter<String> flyashaci = new ArrayAdapter<String>(ACIActivity.this, R.layout.support_simple_spinner_dropdown_item, getResources().getStringArray(R.array.flyash));
        flyashaci.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        flyash.setAdapter(flyashaci);

        flyash.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String text = flyash.getSelectedItem().toString();
                switch (text) {
                    case "None":
                        flyashh="None";
                        break;
                    case "Less than 40":
                        flyashh="<40";
                        break;
                    case "Greater than or equal to 40":
                        flyashh=">=40";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(ACIActivity.this, "Please Select Flyash", Toast.LENGTH_SHORT).show();
            }
        });
        ArrayAdapter<String> slagaci = new ArrayAdapter<String>(ACIActivity.this, R.layout.support_simple_spinner_dropdown_item, getResources().getStringArray(R.array.slag));
        slagaci.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        slag.setAdapter(slagaci);

        slag.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String text = slag.getSelectedItem().toString();
                switch (text) {
                    case "None":
                        slagg="None";
                        break;
                    case "Lesser than 70":
                       slagg="<70";
                        break;
                    case "Greater than or equal to 70":
                       slagg=">=70";
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(ACIActivity.this, "Please Select Slag", Toast.LENGTH_SHORT).show();
            }
        });
        ArrayAdapter<String> ivdacii = new ArrayAdapter<String>(ACIActivity.this, R.layout.support_simple_spinner_dropdown_item, getResources().getStringArray(R.array.ivd));
        ivdacii.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        ivd.setAdapter(ivdacii);

        ivd.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String text = ivd.getSelectedItem().toString();
                switch (text) {
                    case "Greater than 1.2":
                        ivdaci=">1.2";

                        break;
                    case "Less than or equal to 1.2":
                        ivdaci="<=1.2";
                        break;
                    case "Any Type":
                        ivdaci="Any Type";
                        break;


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(ACIActivity.this, "Please Select ivd", Toast.LENGTH_SHORT).show();
            }
        });
        ArrayAdapter<String> elee = new ArrayAdapter<String>(ACIActivity.this, R.layout.support_simple_spinner_dropdown_item, getResources().getStringArray(R.array.element));
        elee.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        ele.setAdapter(elee);

        ele.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String text = ele.getSelectedItem().toString();
                switch (text) {
                    case "Walls":
                        element="WALLS";
                        break;
                    case "Columns":
                        element="COLUMNS";
                        break;
                    case "Any Type":
                        element="ANY";
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(ACIActivity.this, "Please Select Element", Toast.LENGTH_SHORT).show();
            }
        });
        ArrayAdapter<String> cementt= new ArrayAdapter<String>(ACIActivity.this, R.layout.support_simple_spinner_dropdown_item, getResources().getStringArray(R.array.cement));
        cementt.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        cement.setAdapter(cementt);

        cement.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String text = cement.getSelectedItem().toString();
                switch (text) {
                    case "Type1":
                        cementaci="Type 1";
                        break;
                    case "Type2":
                        cementaci="Type 2";
                        break;
                    case "Type3":
                        cementaci="Type 3";
                        break;
                    case "Any Type":
                        cementaci="Any Type";
                        break;


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(ACIActivity.this, "Please Select Cement", Toast.LENGTH_SHORT).show();
            }
        });

        ArrayAdapter<String> heightt= new ArrayAdapter<String>(ACIActivity.this, R.layout.support_simple_spinner_dropdown_item, getResources().getStringArray(R.array.wallheight));
        heightt.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        wh.setAdapter(heightt);

        wh.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String text = wh.getSelectedItem().toString();
                switch (text) {

                    case "Greater than 4.2":
                        whh=">4.2";
                        break;
                    case "Less Than or equal to 4.2":
                        whh="<=4.2";
                        break;
                    case "Any Type":
                        whh="Any Type";
                        break;


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(ACIActivity.this, "Please Select WallHeight", Toast.LENGTH_SHORT).show();
            }
        });



        calc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(volaci.getText().length()==0 || poh.getText().length()==0  ||
                        denaci.getText().length()==0  ||paaci.getText().length()==0  )
                {
                    Toast.makeText(getApplicationContext(),"Please verify the fields",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    final String wallelement=element;
                    final String wallcement=cementaci;
                    final String wallinternal=ivdaci;
                    final String wallslag=slagg;
                    final String wallflyash=flyashh;
                    final String wallretarders=retarders;
                    final String wallslump=slumpp;
                    final String wallheighting=whh;

                    final double wallvolume=Double.parseDouble(volaci.getText().toString());
                    final double wallformheight=Double.parseDouble(poh.getText().toString());
                    final double walldensity=Double.parseDouble(denaci.getText().toString());
                    final double wallplanarea=Double.parseDouble(paaci.getText().toString());


                    AsyncHttpClient client = new AsyncHttpClient();
                    JSONObject jsonObject = new JSONObject();
                    StringEntity entity = null;

                    ACIWallsData aciWallsData=new ACIWallsData(
                            wallelement,
                            wallcement,
                            wallinternal,
                            wallslag,
                            wallflyash,
                            wallretarders,
                            wallslump,
                            wallheighting,
                            walldensity,
                            wallvolume,
                            wallformheight,
                            wallplanarea

                    );


                    SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("retarders",String.valueOf(aciWallsData.getRetarders()));
                    editor.putString("element",String.valueOf(aciWallsData.getElementType()));
                    editor.putString("ivd",String.valueOf(aciWallsData.getInternalVibration()));
                    editor.putString("slag",String.valueOf(aciWallsData.getSlag()));
                    editor.putString("slump",String.valueOf(aciWallsData.getSlump()));
                    editor.putString("flyash",String.valueOf(aciWallsData.getFlyash()));
                    editor.putString("wallheight",String.valueOf(aciWallsData.getWallheight()));
                    editor.putString("density",String.valueOf(aciWallsData.getDen()));
                    editor.putString("volume",String.valueOf(aciWallsData.getVolume()));
                    editor.putString("planarea",String.valueOf(aciWallsData.getPlanareas()));
                    editor.putString("vfh",String.valueOf(aciWallsData.getVerticalformheight()));
                    editor.commit();



                    try
                    {
                        entity=new StringEntity("{}");
                        jsonObject.put("jobID",preferences.getInt("Jobid",0));
                        jsonObject.put("userId",preferences.getInt("userID",0));
                        jsonObject.put("latitude",preferences.getString("Latitude",null));
                        jsonObject.put("longtiude",preferences.getString("Longitude",null));
                        jsonObject.put("retarders",aciWallsData.getRetarders());
                        jsonObject.put("elementType",aciWallsData.getElementType());
                        jsonObject.put("cementType",aciWallsData.getCementType());
                        jsonObject.put("internalVibration",aciWallsData.getInternalVibration());
                        jsonObject.put("slag",aciWallsData.getSlag());
                        jsonObject.put("flyash",aciWallsData.getFlyash());
                        jsonObject.put("slump",aciWallsData.getSlump());
                        jsonObject.put("wallheight",aciWallsData.getWallheight());
                        jsonObject.put("den",aciWallsData.getDen());
                        jsonObject.put("verticalformheight",aciWallsData.getVerticalformheight());
                        jsonObject.put("volume",aciWallsData.getVolume());
                        jsonObject.put("planareas",aciWallsData.getPlanareas());
                        //jsonObject.put("temperatureee",aciWallsData.getTemperatureee());
                        //jsonObject.put("rateofplacement",aciWallsData.getWallrop());


                        entity = new StringEntity(jsonObject.toString());
                        Toast.makeText(ACIActivity.this, "Result calculated successfully", Toast.LENGTH_SHORT).show();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    client.post(ACIActivity.this, "http://rmkeclandt-env.miurmysbmy.us-east-2.elasticbeanstalk.com/aci", entity, "application/json", new AsyncHttpResponseHandler() {
                        //client.get(Walls.this, "http://10.1.2.51:8080/DemoServlet", entity, "application/json", new AsyncHttpResponseHandler(){
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            try {



                                Log.d("Demo Success: ", new JSONObject(new String(responseBody, "UTF-8")).toString());
                                JSONObject jsonObject = new JSONObject(new String(responseBody, "UTF-8"));
                                ACIWallsResponse aciWallsResponse = new ACIWallsResponse(jsonObject.getDouble("ccpmax"));
                                Log.d("Demo ccPmax: ", aciWallsResponse.toString());
                                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                SharedPreferences.Editor editor = settings.edit();
                                editor.putInt("Aci_id", jsonObject.getInt("aciID"));
                                editor.putString("ccpmax",String.valueOf(jsonObject.getDouble("ccpmax")));
                                editor.putString("temperature",String.valueOf("temperature"));
                                editor.commit();
                                ccp.setText(Double.toString(aciWallsResponse.getCcpmax()));
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            try {
                                JSONObject jsonObject = new JSONObject(new String(responseBody, "UTF-8"));
                                Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });


        vir.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ACIActivity.this, ResultAci.class);
                startActivity(intent);
            }
        });





    }


}

