package com.example.ctc.Beans;

public class UserData {

    public String UserName;
    public String Password;
    public int Role;


    public UserData(String username, String password, int role) {
        super();
        this.UserName = username;
        this.Password = password;
        this.Role = role;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public int getRole() {
        return Role;
    }

    public void setRole(int role) {
        Role = role;
    }
}
