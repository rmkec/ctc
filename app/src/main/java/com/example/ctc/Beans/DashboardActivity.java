package com.example.ctc.Beans;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.ctc.Activity.CiriaActivity;

import com.example.ctc.List.ListCiria;

import com.example.ctc.List.ListAci;

import com.example.ctc.R;
import com.example.ctc.Activity.ACIActivity;

import com.example.ctc.sign_in;


public class DashboardActivity extends AppCompatActivity {

    CardView Aci,Ciria,Reportaci,Reportciria,Logout;
    ProgressBar progressBar1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        final SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

       Aci=findViewById(R.id.aci);
       Ciria=findViewById(R.id.ciria);
       Reportciria=findViewById(R.id.ciriareport);
       Reportaci=findViewById(R.id.acireports);
       Logout=findViewById(R.id.logout);


       Aci.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Toast.makeText(DashboardActivity.this,"You selected ACI",Toast.LENGTH_LONG).show();
               Intent intent=new Intent(DashboardActivity.this, ACIActivity.class);
               startActivity(intent);
           }
       });
       Ciria.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Toast.makeText(DashboardActivity.this,"You selected CIRIA",Toast.LENGTH_LONG).show();
               Intent intent=new Intent(DashboardActivity.this, CiriaActivity.class);
               startActivity(intent);
           }
       });
        Reportciria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DashboardActivity.this,"You selected CIRIA Reports",Toast.LENGTH_LONG).show();
                Intent intent=new Intent(DashboardActivity.this, ListCiria.class);
                startActivity(intent);
            }
        });
        Reportaci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DashboardActivity.this,"You selected ACI Reports",Toast.LENGTH_LONG).show();
                Intent intent=new Intent(DashboardActivity.this, ListAci.class);
                startActivity(intent);
            }
        });
        Logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DashboardActivity.this,"Session Over",Toast.LENGTH_LONG).show();
                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);

            }
        });

    }


}
