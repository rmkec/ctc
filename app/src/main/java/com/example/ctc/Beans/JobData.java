package com.example.ctc.Beans;



public class JobData  {

    public String jobcode;
    public int userID;
    public String cluster;
    public String jobName;
    public String jobLocation;
    public String nameOfStructure;
    public String nameofElement;
    public int duration;

    public JobData() {
        super();
    }




    public JobData(String jobcode, String cluster, String jobName, String jobLocation,
                          String nameOfStructure, String nameofElement, int duration) {
        super();
        this.jobcode = jobcode;

        this.cluster = cluster;
        this.jobName = jobName;
        this.jobLocation = jobLocation;
        this.nameOfStructure = nameOfStructure;
        this.nameofElement = nameofElement;
        this.duration = duration;
    }

    public String getJobcode() {
        return jobcode;
    }

    public void setJobcode(String jobcode) {
        this.jobcode = jobcode;
    }


    public String getCluster() {
        return cluster;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobLocation() {
        return jobLocation;
    }

    public void setJobLocation(String jobLocation) {
        this.jobLocation = jobLocation;
    }

    public String getNameOfStructure() {
        return nameOfStructure;
    }

    public void setNameOfStructure(String nameOfStructure) {
        this.nameOfStructure = nameOfStructure;
    }

    public String getNameofElement() {
        return nameofElement;
    }

    public void setNameofElement(String nameofElement) {
        this.nameofElement = nameofElement;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }





}
