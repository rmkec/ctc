package com.example.ctc.List;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.inputmethod.EditorInfo;
import android.widget.Adapter;
import android.widget.SearchView;
import android.widget.Toast;


import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.ctc.Adapter.AciAdapter;
import com.example.ctc.Item.AciItem;
import com.example.ctc.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class ListAci extends AppCompatActivity {

    RecyclerView AciRecylerView;
    AciAdapter aciAdapter;
    List<AciItem> aciresponse;
    RequestQueue mRequestQueue1;
    AsyncHttpClient client = new AsyncHttpClient();
    JSONObject jsonObject = new JSONObject();
    StringEntity entity = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_aci);
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        AciRecylerView = findViewById(R.id.aciRecyclerView);
        aciresponse = new ArrayList<>();
        mRequestQueue1= Volley.newRequestQueue(this);
        try {
            entity = new StringEntity("{}");
            jsonObject.put("userId", preferences.getInt("userID", 0));
            entity = new StringEntity(jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        client.post(ListAci.this, "http://rmkeclandt-env.miurmysbmy.us-east-2.elasticbeanstalk.com/getallacidetailsofauser", entity, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    JSONObject jsonObject = new JSONObject(new String(responseBody));
                    int acicount = jsonObject.getInt("count");
                    JSONArray aciArray = jsonObject.getJSONArray("data");
                    List<AciItem> aciresponse = new ArrayList<>();
                    for (int i = 0; i < acicount; i=i+2) {
                        AciItem itemaci = new AciItem();
                        JSONObject obj = aciArray.getJSONObject(i);
                        itemaci.setAciID(obj.getInt("aciID"));
                        itemaci.setJobid2(obj.getInt("jobID"));
                        itemaci.setJobCode2(obj.getInt("jobCode"));
                        itemaci.setUserId2(obj.getInt("userId"));
                        itemaci.setFormulaVersion2(obj.getString("formulaVersion"));
                        itemaci.setElementType2(obj.getString("elementType"));
                        itemaci.setCementType2(obj.getString("cementType"));
                        itemaci.setInternalVibration2(obj.getString("internalVibration"));
                        itemaci.setSlag2(obj.getString("slag"));
                        itemaci.setFlyash2(obj.getString("flyash"));
                        itemaci.setRetarders2(obj.getString("retarders"));
                        itemaci.setSlump2(obj.getString("slump"));
                        itemaci.setWallheight2(obj.getString("wallheight"));
                        itemaci.setLatitude2(obj.getString("latitude"));
                        itemaci.setLongtiude2(obj.getString("longtiude"));
                        itemaci.setCreatedon2(obj.getString("createdON"));
                        itemaci.setDen2(obj.getInt("den"));
                        itemaci.setVolume2(obj.getInt("volume"));
                        itemaci.setVerticalformheight2(obj.getInt("verticalformheight"));
                        itemaci.setPlanareas2(obj.getInt("planareas"));
                        itemaci.setTemperatureee2(obj.getInt("temperatureee"));
                        itemaci.setPmax2(obj.getInt("pmax"));
                        itemaci.setJobCode2(obj.getInt("jobCode"));
                        itemaci.setUserId2(obj.getInt("userID"));
                        itemaci.setCluster2(obj.getString("cluster"));
                        itemaci.setJobname2(obj.getString("jobName"));
                        itemaci.setJoblocation2(obj.getString("jobLocation"));
                        itemaci.setNameofstructure2(obj.getString("nameOfStructure"));
                        itemaci.setNameofelement2(obj.getString("nameofElement"));
                        itemaci.setDuration2(obj.getInt("duration"));
                        itemaci.setName2(obj.getString("name"));
                        itemaci.setBranch2(obj.getString("branch"));
                        itemaci.setUsername2(obj.getString("userName"));
                        itemaci.setRole(obj.getInt("role"));
                        itemaci.setJobCode2(obj.getInt("jobcode"));
                        aciresponse.add(itemaci);


                    }


                    aciAdapter = new AciAdapter(ListAci.this, aciresponse);
                    AciRecylerView.setAdapter(aciAdapter);
                    AciRecylerView.setLayoutManager(new LinearLayoutManager(ListAci.this));



                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            public boolean onCreateOptionsMenu(Menu menu) {
                MenuInflater Inflater = getMenuInflater();
                Inflater.inflate(R.menu.example_menu,menu);
                MenuItem searchItem = menu.findItem(R.id.action_search);
                SearchView searchView= (SearchView) searchItem.getActionView();
                searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newtext) {
                        aciAdapter.getFilter().filter(newtext);
                        return false;
                    }
                });
                return true;


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                try {
                    JSONObject jsonObject = new JSONObject(new String(responseBody, "UTF-8"));
                    Toast.makeText(getApplicationContext(), jsonObject.getString("details"), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            }
        });
    }
}