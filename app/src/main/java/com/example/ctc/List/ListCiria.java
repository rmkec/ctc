package com.example.ctc.List;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.ctc.Activity.ACIActivity;
import com.example.ctc.Adapter.CiriaAdapter;
import com.example.ctc.Adapter.CiriaAdapter1;
import com.example.ctc.Beans.DashboardActivity;
import com.example.ctc.Item.CiriaItems;
import com.example.ctc.R;

import java.util.ArrayList;
import java.util.List;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.ctc.Adapter.JobsAdapter;
import com.example.ctc.Item.JobItem;
import com.example.ctc.R;
import com.example.ctc.Response.CiriaResponse;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;


public class ListCiria extends AppCompatActivity {
    RecyclerView ciriaRecylerView;
    CiriaAdapter ciriaAdapter;
    List<CiriaItems> ciriaresponse;
    RequestQueue mRequestQueue1;
    AsyncHttpClient client = new AsyncHttpClient();
    JSONObject jsonObject = new JSONObject();
    StringEntity entity = null;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_ciria);
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        ciriaRecylerView = findViewById(R.id.ciriaRecyclerView);
        ciriaresponse = new ArrayList<>();
        mRequestQueue1= Volley.newRequestQueue(this);

        try {
            entity = new StringEntity("{}");
            jsonObject.put("userId", preferences.getInt("userID", 0));
            entity = new StringEntity(jsonObject.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        client.post(ListCiria.this, "http://rmkeclandt-env.miurmysbmy.us-east-2.elasticbeanstalk.com/getallciriadetailsofauser", entity, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    JSONObject jsonObject = new JSONObject(new String(responseBody));
                    int ciriacount =  jsonObject.getInt("count");
                    JSONArray ciriaArray =  jsonObject.getJSONArray("data");
                    List<CiriaItems> ciriaresponse = new ArrayList<>();

                    for (int i = 0; i<ciriacount;i=i+2) {
                        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = settings.edit();


                        CiriaItems items1;
                        items1 = new CiriaItems();
                        JSONObject obj = ciriaArray.getJSONObject(i);
                        items1.setCiriaID1(obj.getInt("ciriaID"));
                        items1.setJobID1(obj.getInt("jobID"));
                        items1.setUserID1(obj.getInt("userId"));
                        items1.setLatitude1(obj.getString("latitude"));
                        items1.setLongtitude1(obj.getString("longtitude"));
                        items1.setFormulaversion1(obj.getString("formulaversion"));
                        items1.setElementt1(obj.getDouble("elementt"));
                        items1.setC21(obj.getDouble("c2"));
                        items1.setDensity1(obj.getDouble("density"));
                        items1.setVerticalFormHeight1(obj.getDouble("verticalFormHeight"));
                        items1.setVerticalPourHeight1(obj.getDouble("verticalPourHeight"));
                        items1.setVolume1(obj.getDouble("volume"));
                        items1.setPlanArea1(obj.getDouble("planArea"));
                        items1.setTemperature1(obj.getDouble("temperature"));
                        items1.setPmax1(obj.getDouble("pmax"));
                        items1.setCreatedON1(obj.getString("createdON"));
                        items1.setJobCode1(obj.getString("jobcode"));
                        items1.setCluster1(obj.getString("cluster"));
                        items1.setJobName1(obj.getString("jobName"));
                        items1.setJobLocation1(obj.getString("jobLocation"));
                        items1.setNameOfStructure1(obj.getString("nameOfStructure"));
                        items1.setNameofElement1(obj.getString("nameofElement"));
                        items1.setName1(obj.getString("name"));
                        items1.setBranch1(obj.getString("branch"));
                        items1.setUserName1(obj.getString("userName"));
                        items1.setRole1(obj.getInt("role"));
                        items1.setUserID2(obj.getInt("userID"));
                        ciriaresponse.add(items1);


                        /*editor.putString("CiriaC1",String.valueOf(items1.getCiriaID1()));
                        editor.putString("CiriaC2",String.valueOf(items1.getC21()));
                        editor.putString("Ciriaden",String.valueOf(items1.getDensity1()));
                        editor.putString("Ciriafh",String.valueOf(items1.getVerticalFormHeight1()));
                        editor.putString("Ciriaph",String.valueOf(items1.getVerticalPourHeight1()));
                        editor.putString("Ciriavol",String.valueOf(items1.getVolume1()));
                        editor.putString("Ciriapa",String.valueOf(items1.getPlanArea1()));
                        editor.putString("Ciriatemp",String.valueOf(items1.getTemperature1()));
                        editor.putString("Ciriapmax",String.valueOf(items1.getPmax1()));
                        editor.putString("Ciriajc",String.valueOf(items1.getJobCode1()));
                        editor.putString("Ciriajn",items1.getLatitude1());
                        editor.putString("Ciriajl",items1.getLongtitude1());
                        editor.putString("Ciriaun",String.valueOf(items1.getUserId1()));
                        editor.commit();*/



                    }
                    ciriaAdapter = new CiriaAdapter(ListCiria.this, ciriaresponse);
                    ciriaRecylerView.setAdapter(ciriaAdapter);
                    ciriaRecylerView.setLayoutManager(new LinearLayoutManager(ListCiria.this));





                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }



            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                try {
                    JSONObject jsonObject = new JSONObject(new String(responseBody, "UTF-8"));
                    Toast.makeText(getApplicationContext(), jsonObject.getString("details"), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            }

        });



    }


}
