package com.example.ctc;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import android.widget.Toast;

import com.example.ctc.Beans.DashboardActivity;
import com.example.ctc.Beans.JobData;
import com.example.ctc.List.ListJobs;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class JobDetails extends AppCompatActivity {

    EditText jobCode, jobName, jobLocation, structureName, elementName, duration;
    Button jobSaveBtn, viewJobsBtn;
    Spinner clusterSpinnner;
    String selectedCluster;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job);
        final SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        jobCode = findViewById(R.id.jobCode);
        jobName = findViewById(R.id.jobReportName);
        jobLocation = findViewById(R.id.jobLocation);
        structureName = findViewById(R.id.structureName);
        elementName = findViewById(R.id.elementName);
        duration = findViewById(R.id.duration);
        clusterSpinnner = findViewById(R.id.cluster);

        jobSaveBtn = findViewById(R.id.saveJob);
        viewJobsBtn = findViewById(R.id.viewJobs);

        ArrayAdapter<String> roleAdapter = new ArrayAdapter<String>(JobDetails.this, R.layout.support_simple_spinner_dropdown_item, getResources().getStringArray(R.array.cluster));
        roleAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        clusterSpinnner.setAdapter(roleAdapter);

        clusterSpinnner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedCluster = clusterSpinnner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(JobDetails.this, "Please Select Cluster", Toast.LENGTH_SHORT).show();
            }
        });

        jobSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedCluster.length() == 0) {
                    Toast.makeText(getApplicationContext(), "Please Select Cluster", Toast.LENGTH_SHORT).show();
                } else if (jobCode.getText().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Job Code Required", Toast.LENGTH_SHORT).show();
                } else if (jobName.getText().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Job Code Required", Toast.LENGTH_SHORT).show();
                } else if (jobLocation.getText().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Job Code Required", Toast.LENGTH_SHORT).show();
                } else if (structureName.getText().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Job Code Required", Toast.LENGTH_SHORT).show();
                } else if (elementName.getText().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Job Code Required", Toast.LENGTH_SHORT).show();
                } else if (duration.getText().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Job Code Required", Toast.LENGTH_SHORT).show();
                } else {
                    final String Jobcode = jobCode.getText().toString();
                    final String Cluster = selectedCluster;
                    final String Joblocation = jobLocation.getText().toString();
                    final String Jobname = jobName.getText().toString();
                    final String Structure = structureName.getText().toString();
                    final String Element = elementName.getText().toString();
                    final int Duration = Integer.parseInt(duration.getText().toString());

                    AsyncHttpClient client = new AsyncHttpClient();
                    JSONObject jsonObject = new JSONObject();
                    StringEntity entity = null;
                    JobData jobData = new JobData(Jobcode, Cluster, Jobname, Joblocation, Structure, Element, Duration);
                    SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("JobCode", jobData.getJobcode());
                    editor.putString("JobName", jobData.getJobName());
                    editor.putString("JobLocation", jobData.getJobLocation());
                    editor.commit();



                    try {
                        entity = new StringEntity("{}");
                        jsonObject.put("jobcode", jobData.getJobcode());
                        jsonObject.put("userID", getPrefs.getInt("userID", 0));
                        jsonObject.put("cluster", jobData.getCluster());
                        jsonObject.put("jobName", jobData.getJobName());
                        jsonObject.put("jobLocation", jobData.getJobLocation());
                        jsonObject.put("nameOfStructure", jobData.getNameOfStructure());
                        jsonObject.put("nameofElement", jobData.getNameofElement());
                        jsonObject.put("duration", jobData.getDuration());
                        entity = new StringEntity(jsonObject.toString());
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    client.post(JobDetails.this, "http://rmkeclandt-env.miurmysbmy.us-east-2.elasticbeanstalk.com/postjobdetails", entity, "application/json", new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            try {
                                JSONObject jsonObject = new JSONObject(new String(responseBody, "UTF-8"));
                                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                SharedPreferences.Editor editor = settings.edit();
                                editor.putInt("Jobid", jsonObject.getInt("jobID"));
                                editor.commit();
                                Toast.makeText(JobDetails.this, "Details added succesfully", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(JobDetails.this, DashboardActivity.class);
                                startActivity(intent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            try {
                                JSONObject jsonObject = new JSONObject(new String(responseBody, "UTF-8"));
                                Toast.makeText(getApplicationContext(), jsonObject.getString("details"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }

            }
        });

        viewJobsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent = new Intent(JobDetails.this, ListJobs.class);
                startActivity(intent);
            }


        });

    }
}
